using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace notes
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

           // app.MapWhen(context => context.Request.Method == HttpMethods.Get, appBuilder =>
           //appBuilder.Run(async context =>
           //{
           //    await context.Response.WriteAsync(File.ReadAllText(@"C:\Users\alyona\Documents\informatics_c#\notes\notes\Pages\home.html"));
           //}));

            app.MapWhen(context => context.Request.Method == HttpMethods.Post, appBuilder =>
            appBuilder.Run(async context =>
            {
                var title = context.Request.Form["title"];
                var text = context.Request.Form["text"];
                File.WriteAllLines(@"C:\Users\alyona\Documents\informatics_c#\notes\notes\" + title, text);
                await context.Response.WriteAsync(title + text);
            }
            ));


            app.Run(async context =>
            {
                await context.Response.WriteAsync(File.ReadAllText(@"C:\Users\alyona\Documents\informatics_c#\notes\notes\Pages\home.html"));
            });
        }
    }
}
